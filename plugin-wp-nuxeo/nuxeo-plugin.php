<?php
/*
Plugin Name: Plugin Nuxeo for Wordpress
Plugin URI: https://github.com/nuxeo/plugin-wp-nuxeo
Description: Wordpress Plugin for Nuxeo through Automation API. Tested with Nuxeo 6.0.
Version: 0.0.1-snapshot
Author: nuxeo
Author URI: https://github.com/nuxeo

 * (C) Copyright 2015 Nuxeo SA (http://nuxeo.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Laurent Dreuillat
 */
// Required PHP files
$PLUGIN_PATH = dirname(__FILE__).'/';
require_once($PLUGIN_PATH . '/admin.php');
require_once ($PLUGIN_PATH . '/stream.php');
require_once ($PLUGIN_PATH . '/nuxeo/NuxeoAutomationAPI.php');

class nuxeo_shortcode {
	public $shortcode_tag = 'nuxeo';
	
	function __construct($args = array()) {
		//add shortcode
		add_shortcode( $this->shortcode_tag, array( $this, 'nuxeo_shortcode_handler' ) );
		
		if ( is_admin() ){
			add_action('admin_head', array( $this, 'admin_head') );
			add_action( 'admin_enqueue_scripts', array($this , 'admin_enqueue_scripts' ) );
		}
	}
	
	/*
	Define Nuxeo shortcode
	
	Usage:
	    [nuxeo path="/my particular/folder name/"] 												# Display the content of a folderish document based on its path (under the configured domain path)
	    [nuxeo type="File"]   																							# Display Documents whose type matches
	    [nuxeo nxquery="SELECT * FROM File WHERE ..."]                				# Display Documents based on the NXQL query
	    [nuxeo name="Agenda%.doc"]                   													# Docs whose name matches. May include wildcard character '%'.
		[nuxeo doc_id="c5e890d3-87f5-42cb-abe4-1ed9d0d2600c"]				# Display the detail of a document based on its id
		[nuxeo folderish_id="05cdeaa1-e57d-450a-8347-1600206e7cce"]		# Display the content of a folderish document based on its id
	*/
	function nuxeo_shortcode_handler( $attr, $content = null ) {
	    extract( shortcode_atts( array(
	      'path' => '',
	      'type' => '',
		  'nxquery' => '',
	      'name' => '',
		  'doc_id' => '',
		  'folderish_id' => ''
	      ), $attr ) );
		
		// Escape sur les caractères posant problème
		$tmp = $folder;
		$folder = str_replace("'", "\'", $tmp);
		
		return $this->do_nxql($path, $type, $nxquery, $name, $doc_id, $folderish_id);
	}
	
	function do_nxql($path, $type, $nxquery, $name, $doc_id, $folderish_id) {
		// Initialize Nuxeo Client
	    $repo_url = get_option('nx_repository_url');
		$workspace_root = get_option('nx_workspace_root');
	    //$repo_username = get_option('nx_username');
		$current_user = wp_get_current_user();	
		$repo_username = $current_user->user_login;
	    $repo_password = get_option('nx_password');
		$display_nxql = get_option('nx_display_query');
		$error_message = 'Unable to display documents.';
		$folder_url = $workspace_root;
		
		// Check si appel d'un document ou dossier directement depuis son permalink
		if ($nxquery) {
				$result = $this->display_nuxeo_query($repo_url, $repo_username, $repo_password, $nxquery, $display_nxql);
		} elseif ($doc_id) {
			$result = $this->display_nuxeo_document_by_Id($repo_url, $repo_username, $repo_password, $doc_id, $display_nxql);
		} elseif ($folderish_id) {
			$result = $this->display_nuxeo_folderish_by_Id($repo_url, $repo_username, $repo_password, $folderish_id, $name, $type, $display_nxql);
		} else {	
			// Add domain path if specified
			if ($workspace_root) {
				$path = $workspace_root.'/'.$path;
			}
			
			$result = $this->display_nuxeo_document($repo_url, $repo_username, $repo_password, $path, $type, $name, $display_nxql);
		}
		
		return $result;
	}
	
	function admin_head() {
		// check user permissions
		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
			return;
		}
	
		// check if WYSIWYG is enabled
		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'mce_external_plugins', array( $this ,'mce_external_plugins' ) );
			add_filter( 'mce_buttons', array($this, 'mce_buttons' ) );
		}
	}
	
	function mce_external_plugins( $plugin_array ) {
		$plugin_array["nuxeo"] = plugins_url( 'js/mce-button.js' , __FILE__ );
		return $plugin_array;
	}
	
	function mce_buttons( $buttons ) {
		array_push( $buttons, "nuxeo" );
		return $buttons;
	}
	
	function admin_enqueue_scripts(){
		wp_enqueue_style('nuxeo_shortcode', plugins_url( 'css/mce-button.css' , __FILE__ ) );
	}
	
	/**
	 * Function that display documents details based on a NXQL Query
	 * @param unknown $repo_url
	 * @param unknown $repo_username
	 * @param unknown $repo_password
	 * @param unknown $doc_id
	 * @param unknown $display_nxql
	 * @return string
	 */
	function display_nuxeo_query($repo_url, $repo_username, $repo_password, $nxquery, $display_nxql) {
		
		$nxclient = new NuxeoPhpAutomationClient($repo_url.'/site/automation');
		$nxsession = $nxclient->getSession($repo_username, $repo_password);
	
		$msg = $msg . $this->trace_nuxeo_query($nxquery, $display_nxql);
	
		$docList = $nxsession->newRequest("Document.Query")->set('params', 'query', "$nxquery")->setSchema($schema = '*')->sendRequest();
		$docArray = $docList->getDocumentList();
		$value = sizeof($docArray);
		$msg = $msg.$this->getDisplayHeader();
		for ($test = 0; $test < $value; $test++) {
			$msg = $msg . $this->display_nuxeo_object(current($docArray));
			next($docArray);
		}
		$msg = $msg . $this->getDisplayFooter();
	
		return $msg;
	}
	
	function display_nuxeo_document_by_Id($repo_url, $repo_username, $repo_password, $doc_id, $display_nxql) {
		$nxclient = new NuxeoPhpAutomationClient($repo_url.'/site/automation');
		$nxsession = $nxclient->getSession($repo_username, $repo_password);
		
		$query = "SELECT * FROM Document WHERE ecm:uuid = '$doc_id'";
		
		$query = $query." AND ecm:mixinType != 'Folderish' AND ecm:mixinType != 'HiddenInNavigation' AND ecm:isCheckedInVersion = 0 AND ecm:currentLifeCycleState != 'deleted'";
		
		$msg = $msg . $this->trace_nuxeo_query($query, $display_nxql);
		
		$docList = $nxsession->newRequest("Document.Query")->set('params', 'query', "$query")->setSchema($schema = '*')->sendRequest();
		$docArray = $docList->getDocumentList();
		$value = sizeof($docArray);
		$msg = $msg.$this->getDisplayHeader();
		
		for ($test = 0; $test < $value; $test++) {
			$msg = $msg . $this->display_nuxeo_object(current($docArray));
			next($docArray);
		}
		$msg = $msg . $this->getDisplayFooter();
	
		return $msg;
	}
	
	function display_nuxeo_folderish_by_Id($repo_url, $repo_username, $repo_password, $dossier_id, $name, $type, $display_nxql) {
		$nxclient = new NuxeoPhpAutomationClient($repo_url.'/site/automation');
		$nxsession = $nxclient->getSession($repo_username, $repo_password);
		
		$query = "SELECT * FROM Document WHERE ecm:parentId = '$dossier_id'";
		
		if ($name) {
			$query = $query." AND dc:title LIKE '$name%'";
		}
		
		if ($name) {
			$query = $query." AND ecm:primaryType = '$type%'";
		}
		
		$query = $query." AND ecm:mixinType != 'Folderish' AND ecm:mixinType != 'HiddenInNavigation' AND ecm:isCheckedInVersion = 0 AND ecm:currentLifeCycleState != 'deleted'";
		
		$msg = $msg . $this->trace_nuxeo_query($query, $display_nxql);
		
		$docList = $nxsession->newRequest("Document.Query")->set('params', 'query', "$query")->setSchema($schema = '*')->sendRequest();
		$docArray = $docList->getDocumentList();
		$value = sizeof($docArray);
		$msg = $msg.$this->getDisplayHeader();
		
		for ($test = 0; $test < $value; $test++) {
			$msg = $msg . $this->display_nuxeo_object(current($docArray));
			next($docArray);
		}
		$msg = $msg . $this->getDisplayFooter();
		
		return $msg;
	}
	
	function display_nuxeo_document($repo_url, $repo_username, $repo_password, $path, $type, $name, $display_nxql) {
		$nxclient = new NuxeoPhpAutomationClient($repo_url.'/site/automation');
		$nxsession = $nxclient->getSession($repo_username, $repo_password);
		
		$query = "SELECT * FROM Document WHERE";
		
		if ($path) {
			$query = $query." ecm:path STARTSWITH '$path'";
			
			if ($name || $type) {
				$query = $query." AND";
			}
		}
		
		if ($name) {
			$query = $query." dc:title LIKE '$name%'";
			
			if ($type) {
				$query = $query." AND";
			}
		}
		
		if ($type) {
			$query = $query." ecm:primaryType = '$type'";
		}
		
		$query = $query." AND ecm:mixinType != 'Folderish' AND ecm:mixinType != 'HiddenInNavigation' AND ecm:isCheckedInVersion = 0 AND ecm:currentLifeCycleState != 'deleted'";
		
		$msg = $msg . $this->trace_nuxeo_query($query, $display_nxql);
		
		$docList = $nxsession->newRequest("Document.Query")->set('params', 'query', "$query")->setSchema($schema = '*')->sendRequest();
		$docArray = $docList->getDocumentList();
		$value = sizeof($docArray);
		$msg = $msg.$this->getDisplayHeader();
		
		for ($test = 0; $test < $value; $test++) {
			$msg = $msg . $this->display_nuxeo_object(current($docArray));
			next($docArray);
		}
		$msg = $msg . $this->getDisplayFooter();
	
		return $msg;
	}
	
	/**
	 * Function that display the detail of the current NXQL Query if the trace option is activated
	 * @param unknown $query NXQL Query to display
	 * @param unknown $display_nxql Trace option
	 */
	function trace_nuxeo_query($query, $display_nxql) {
		if ($display_nxql != "false") {
			$trace = $trace . '<p><font color=\'red\'>Nuxeo Query : ' . $query . '</font></p>';
		} else {
			$trace = "";
		}
		
		return $trace;
	}
	
	function getDisplayHeader() {
		$header = $header . '<table border=\'1\'>';
		$header = $header.'<tr bgcolor=\'#0084C3\'>';
		$header = $header.'<th>Doc</th><th>Creator</th><th>Creation date</th><th>Type</th>';
		$header = $header.'</tr>';	
		
		return $header;
	}
	
	function getDisplayFooter() {
		$footer = $footer . '</table>';
		
		return $footer;
	}
	
	function display_nuxeo_object($nxDoc) {
		$nxUtils = new NuxeoUtilities();
		$phpDate = $nxUtils->dateConverterNuxeoToPhp($nxDoc->getProperty('dc:created'));
		$nxUrl = $this->get_nxdoc_url($nxDoc);
		
		$msg = $msg . '<tr>';
		$msg = $msg . '<td><a href="'.$nxUrl.'">'.$nxDoc->getTitle().'</a></td>';
		$msg = $msg . '<td>'.$nxDoc->getProperty('dc:creator').'</td>';
		$msg = $msg . '<td>'.$phpDate->format('d-m-Y').'</td>';
		$msg = $msg . '<td>'.$nxDoc->getType().'</td>';
		$msg = $msg . '</tr>';
		
		return $msg;
	}
	
	function get_nxdoc_url($nxDoc) {
		$siteurl = get_option('siteurl');
	    $id = $nxDocUrl.$nxDoc->getUid();
		$fileName=$nxDoc->getProperty('file:filename');
	    return $siteurl . '?nx_doc_id=' . urlencode($id).'&nx_file_name='.$fileName;
	}
}

new nuxeo_shortcode();

?>